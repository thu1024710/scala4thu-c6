package cc

/**
  * Created by mac046 on 2017/4/17.
  */
case class Round(r:Double){
  def area()=math.pow(r,2)* math.Pi
}
def circu()={
  2*r*scala.math.Pi
}
}
